package com.androidexample.customspinner; 

import java.util.ArrayList;
import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;  
 
public class CustomSpinner extends Activity {
 
    public  ArrayList<SpinnerModel> CustomListViewValuesArr = new ArrayList<SpinnerModel>();
    TextView output = null;
    CustomAdapter adapter;
    CustomSpinner activity = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_spinner);
        
        activity  = this;
        
        Spinner  SpinnerExample = (Spinner)findViewById(R.id.spinner);
        output         = (TextView)findViewById(R.id.output);
        
        setListData();
        
        Resources res = getResources(); 
        adapter = new CustomAdapter(activity, R.layout.spinner_rows, CustomListViewValuesArr,res);
        SpinnerExample.setAdapter(adapter);
        
        SpinnerExample.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // your code here
            	
            	String Person    = ((TextView) v.findViewById(R.id.person)).getText().toString();
            	String PersonName = ((TextView) v.findViewById(R.id.sub)).getText().toString();
            	
            	String OutputMsg = "Selected Person : \n\n"+Person+"\n"+PersonName;
            	output.setText(OutputMsg);
            	
            	Toast.makeText(
						getApplicationContext(),OutputMsg, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
 
    /****** Function to set data in ArrayList *************/
    public void setListData()
    {
    	String [] a ={"abc"," Khunanon Chunlakan"," Supalak Sirikutjatuporn"," Chutiya Natkhosit"," Thanchanok Klabsong"," Chatpol Akkawattanakul"," Supanij Leelataweewud"," Kitiwan Wattanajaroenrung"," Supree Srimanchanda",
    			" Korawich Sensathian"," Thanakron Tandavas"," Photpinit Kalayanuwatchai"," Monkawee Noonate"," Phatyawat Asarasri"," Chayaphorn Thongsukchote"," Onjira Benjarangseepornchai"," Tanapol Tangthamvanich"," Setthawut Nimsuk",
    			" Tinnaphob Angkaprasert"," Smith Kalpiyapan"," Punnakhun Bungate"," Mirantee Samranchaikhong"," Vanpen Phongam"," Intawan Songwattana"," Napat Patpituck"};
		
    	String [] b ={"abc"," 5422770842"," 5422770594"," 5422792051", " 5422791889"," 5422792135"," 5422780940"," 5422781211"," 5422791004"," 5422772806"," 5422800136"," 5422770669"," 5422780049"," 5422770362"," 5422770792"," 5422791137"," 5422770404"," 5422772681"," 5422800045"," 5422780700"," 5422770693", " 5422770818"," 5422770511"," 5422770529"," 5422770461"};
    	for (int i = 0; i < 24; i++) {
			
    		
    	
			final SpinnerModel sched = new SpinnerModel();
			    
			  /******* Firstly take data in model object ******/
			   sched.setPersonName("Name:"+a[i]);
			   sched.setImage("image"+i);
			   sched.setId("ID:"+b[i]);
			   
			   
			/******** Take Model Object in ArrayList **********/
			CustomListViewValuesArr.add(sched);
		}
		
    }
    
   }